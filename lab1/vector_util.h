#ifndef VECTOR_UTIL_H
#define VECTOR_UTIL_H

#include <stddef.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_math.h>

double vector_sum(const gsl_vector *v);
void vector_poly_eval(const gsl_vector *polynomial, const gsl_vector *arguments, gsl_vector *result);

inline double vector_sum_sq(const gsl_vector *v, size_t from, size_t to) {
    double sum_sq = 0;

    for (size_t j = from; j <= to; ++j) {
        sum_sq += gsl_pow_2(gsl_vector_get(v, j));
    }

    return sum_sq;
}

#endif // VECTOR_UTIL_H
