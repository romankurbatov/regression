#ifndef CHISQ_H
#define CHISQ_H

#include <gsl/gsl_vector.h>

int test_uniform_chisq(gsl_vector *sample, double alpha);
int test_homogeneity_chisq(gsl_vector *sample, double alpha);

#endif // CHISQ_H
