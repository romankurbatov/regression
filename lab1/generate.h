#ifndef GENERATE_H
#define GENERATE_H

#include <stddef.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_rng.h>

void generate_gaussian_sample(double mu, double sigma, gsl_rng *generator, gsl_vector *sample);
void generate_uniform_sample(gsl_rng *generator, gsl_vector *sample);

#endif // GENERATE_H
