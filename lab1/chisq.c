#include "chisq.h"

#include <math.h>
#include <assert.h>
#include <gsl/gsl_cdf.h>

#include "vector_util.h"

int test_uniform_chisq(gsl_vector *sample, double alpha) {
    const size_t n = sample->size;
    const size_t k = (size_t) round(1.72*pow(n, 1.0/3));

    gsl_vector *delta = gsl_vector_calloc(k);

    for (size_t i = 0; i < n; ++i) {
        const double x = gsl_vector_get(sample, i);

        const double j_real = x * k;
        size_t j;
        if (j_real < 0) {
            j = 0;
        } else {
            j = (size_t) j_real;
            if (j >= k)
                j = k - 1;
        }

        const double old = gsl_vector_get(delta, j);
        gsl_vector_set(delta, j, old + 1);
    }
    gsl_vector_scale(delta, 1.0/n);
    gsl_vector_add_constant(delta, -1.0/k);

    const double chisq = k * n * vector_sum_sq(delta, 0, k - 1);

    const int result = (chisq < gsl_cdf_chisq_Pinv(1 - alpha, k - 1));

    gsl_vector_free(delta);

    return result;
}

int test_homogeneity_chisq(gsl_vector *sample, double alpha) {
    const size_t n = sample->size;
    const size_t half_size = n / 2;
    const size_t k = (size_t) round(1.72*pow(half_size, 1.0/3));

    const double left = gsl_vector_min(sample);
    const double right = gsl_vector_max(sample);

    gsl_vector *points_in_intervals_1 = gsl_vector_calloc(k);
    gsl_vector *points_in_intervals_2 = gsl_vector_calloc(k);

    for (size_t i = 0; i < n; ++i) {
        const double x = gsl_vector_get(sample, i);

        const double j_real = (x - left)/(right - left) * k;
        size_t j;
        if (j_real < 0) {
            j = 0;
        } else {
            j = (size_t) j_real;
            if (j >= k)
                j = k - 1;
        }

        if (i < half_size) {
            const double old = gsl_vector_get(points_in_intervals_1, j);
            gsl_vector_set(points_in_intervals_1, j, old + 1);
        } else {
            const double old = gsl_vector_get(points_in_intervals_2, j);
            gsl_vector_set(points_in_intervals_2, j, old + 1);
        }
    }

    gsl_vector *productions = gsl_vector_alloc(k);
    gsl_vector_memcpy(productions, points_in_intervals_1);
    gsl_vector_mul(productions, points_in_intervals_2);

    gsl_vector *sums = gsl_vector_alloc(k);
    gsl_vector_memcpy(sums, points_in_intervals_1);
    gsl_vector_add(sums, points_in_intervals_2);

    gsl_vector *fractions = gsl_vector_alloc(k);
    gsl_vector_memcpy(fractions, productions);
    gsl_vector_div(fractions, sums);

    const double chisq = n - 4*vector_sum(fractions);

    const int result = (chisq < gsl_cdf_chisq_Pinv(1 - alpha, k - 1));

    gsl_vector_free(fractions);
    gsl_vector_free(sums);
    gsl_vector_free(productions);
    gsl_vector_free(points_in_intervals_2);
    gsl_vector_free(points_in_intervals_1);

    return result;
}
