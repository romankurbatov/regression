#ifndef SARKADI_H
#define SARKADI_H

#include <gsl/gsl_vector.h>

int test_normal_sarkadi(const gsl_vector *sample, double alpha);

#endif // SARKADI_H
