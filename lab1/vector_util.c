#include "vector_util.h"

#include <stdlib.h>
#include <assert.h>
#include <gsl/gsl_poly.h>

double vector_sum(const gsl_vector *v) {
    double sum = 0;

    for (size_t i = 0; i < v->size; ++i) {
        sum += gsl_vector_get(v, i);
    }

    return sum;
}

void vector_poly_eval(const gsl_vector *polynomial, const gsl_vector *arguments, gsl_vector *result) {
    assert(result->size >= arguments->size);

    /*
     * Unfortunately, gsl_poly_eval() works with plain C arrays, not GSL vectors.
     * To use it, I convert polynomial from GSL vector to plain C array.
     */

    double *polynomial_array = malloc(polynomial->size * sizeof(polynomial_array[0]));
    for (size_t i = 0; i < polynomial->size; ++i) {
        polynomial_array[i] = gsl_vector_get(polynomial, i);
    }

    for (size_t i = 0; i < arguments->size; ++i) {
        gsl_vector_set(result, i,
            gsl_poly_eval(polynomial_array, polynomial->size, gsl_vector_get(arguments, i)));
    }

    free(polynomial_array);
}
