#ifndef LATEX_H
#define LATEX_H

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector_int.h>
#include <gsl/gsl_matrix_int.h>

void latex_export_matrix_int(
        const gsl_matrix_int *matrix,
        const char *horizontal_title,
        const char *vertical_title,
        const gsl_vector_int *row_titles,
        const gsl_vector_int *column_titles,
        const char *filename);

void latex_export_matrix(
        const gsl_matrix *matrix,
        const char *horizontal_title,
        const char *vertical_title,
        const gsl_vector_int *row_titles,
        const gsl_vector_int *column_titles,
        const char *filename);

#endif // LATEX_H
