#include "generate.h"

#include <time.h>
#include <gsl/gsl_randist.h>

void generate_gaussian_sample(double mu, double sigma, gsl_rng *generator, gsl_vector *sample) {
    for(size_t i = 0; i < sample->size; ++i) {
        gsl_vector_set(sample, i, mu + gsl_ran_gaussian(generator, sigma));
    }
}

void generate_uniform_sample(gsl_rng *generator, gsl_vector *sample) {
    for(size_t i = 0; i < sample->size; ++i) {
        gsl_vector_set(sample, i, gsl_rng_uniform(generator));
    }
}
