#include "latex.h"

#include <stdio.h>
#include <assert.h>
#include <gsl/gsl_math.h>

void latex_export_matrix_int(
        const gsl_matrix_int *matrix,
        const char *horizontal_title,
        const char *vertical_title,
        const gsl_vector_int *row_titles,
        const gsl_vector_int *column_titles,
        const char *filename) {
    assert(matrix->size1 == row_titles->size);
    assert(matrix->size2 == column_titles->size);

    FILE *file = fopen(filename, "w");
    assert(file != NULL);

    fprintf(file, "\\begin{tabular}{*{%zu}{|c}|}\n", matrix->size2 + 1);
    fprintf(file, "\\hline\n");
    fprintf(file, "\\diagbox{%s}{%s}", vertical_title, horizontal_title);
    for (size_t col = 0; col < column_titles->size; ++col) {
        fprintf(file, "\t&\t%d", gsl_vector_int_get(column_titles, col));
    }
    fprintf(file, "\\\\\n");

    for (size_t row = 0; row < matrix->size1; ++row) {
        fprintf(file, "\\hline\n");
        fprintf(file, "%d", gsl_vector_int_get(row_titles, row));
        for (size_t col = 0; col < matrix->size2; ++col) {
            fprintf(file, "\t&\t%d\\%%", gsl_matrix_int_get(matrix, row, col));
        }
        fprintf(file, "\\\\\n");
    }

    fprintf(file, "\\hline\n");

    fprintf(file, "\\end{tabular}\n");

    fclose(file);
}

void latex_export_matrix(
        const gsl_matrix *matrix,
        const char *horizontal_title,
        const char *vertical_title,
        const gsl_vector_int *row_titles,
        const gsl_vector_int *column_titles,
        const char *filename) {
    assert(matrix->size1 == row_titles->size);
    assert(matrix->size2 == column_titles->size);

    FILE *file = fopen(filename, "w");
    assert(file != NULL);

    fprintf(file, "\\begin{tabular}{*{%zu}{|c}|}\n", matrix->size2 + 1);
    fprintf(file, "\\hline\n");
    fprintf(file, "\\diagbox{%s}{%s}", vertical_title, horizontal_title);
    for (size_t col = 0; col < column_titles->size; ++col) {
        fprintf(file, "\t&\t%d", gsl_vector_int_get(column_titles, col));
    }
    fprintf(file, "\\\\\n");

    for (size_t row = 0; row < matrix->size1; ++row) {
        fprintf(file, "\\hline\n");
        fprintf(file, "%d", gsl_vector_int_get(row_titles, row));
        for (size_t col = 0; col < matrix->size2; ++col) {
            double value = gsl_matrix_get(matrix, row, col);
            if (gsl_isnan(value)) {
                fprintf(file, "\t&\t");
            } else {
                fprintf(file, "\t&\t%.1le", value);
            }
        }
        fprintf(file, "\\\\\n");
    }

    fprintf(file, "\\hline\n");

    fprintf(file, "\\end{tabular}\n");

    fclose(file);
}
