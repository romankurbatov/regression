#include "sarkadi.h"

#include <stddef.h>
#include <math.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_cdf.h>

#include "vector_util.h"
#include "chisq.h"

int test_normal_sarkadi(const gsl_vector *ksi, double alpha) {
    const size_t n = ksi->size;
    const size_t m = n - 1;
    gsl_vector *eta = gsl_vector_alloc(n - 1);

    const double sum_ksi = vector_sum(ksi);
    const double ksi_m = gsl_vector_get(ksi, m);
    for (size_t j = 0; j < n - 1; ++j) {
        const double ksi_j = gsl_vector_get(ksi, j);
        const double eta_j = ksi_j - sum_ksi/(n + sqrt(n)) - ksi_m/(sqrt(n) + 1);
        gsl_vector_set(eta, j, eta_j);
    }

    gsl_vector *zeta = gsl_vector_alloc(n - 2);
    for (size_t j = 0; j < n - 2; ++j) {
        const double eta_j = gsl_vector_get(eta, j);
        const double zeta_j = eta_j / sqrt(vector_sum_sq(eta, j + 1, n - 2) / (n - j - 2));
        gsl_vector_set(zeta, j, zeta_j);
    }

    gsl_vector *delta = gsl_vector_alloc(n - 2);
    for (size_t j = 0; j < n - 2; ++j) {
        const double zeta_j = gsl_vector_get(zeta, j);
        const double delta_j = gsl_cdf_tdist_P(zeta_j, n - j - 2);
        gsl_vector_set(delta, j, delta_j);
    }

    const int result = test_uniform_chisq(delta, alpha);

    gsl_vector_free(delta);
    gsl_vector_free(zeta);
    gsl_vector_free(eta);

    return result;
}
