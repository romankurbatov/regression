#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <assert.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_vector_int.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_matrix_int.h>
#include <gsl/gsl_poly.h>
#include <gsl/gsl_multifit.h>
#include <gsl/gsl_rng.h>

#include "vector_util.h"
#include "generate.h"
#include "chisq.h"
#include "sarkadi.h"
#include "latex.h"

#define N_RUNS 100
#define TIME_FROM 0
#define TIME_TO 10
#define N_SIGMAS 3
#define N_OBSERVATIONS 1000
#define MAX_POWER 6

#define MAX_FILENAME 100

void generate_times(double time_from, double time_to, gsl_vector *times);
void generate_sigmas(const gsl_vector *values_exact, gsl_vector *sigmas);
void calculate_predictor_matrix(const gsl_vector *times, gsl_matrix *X);

int main()
{
    gsl_vector *times = gsl_vector_alloc(N_OBSERVATIONS);
    generate_times(TIME_FROM, TIME_TO, times);

    gsl_rng *generator = gsl_rng_alloc(gsl_rng_mt19937);
    gsl_rng_set(generator, time(NULL));

    gsl_multifit_linear_workspace *workspace = gsl_multifit_linear_alloc(N_OBSERVATIONS, MAX_POWER + 1);
    gsl_vector *values_exact = gsl_vector_alloc(N_OBSERVATIONS);
    gsl_vector *values_exact_experim = gsl_vector_alloc(N_OBSERVATIONS);
    gsl_vector *values = gsl_vector_alloc(N_OBSERVATIONS);
    gsl_vector *sigmas = gsl_vector_alloc(N_SIGMAS);
    gsl_vector *errors_theor = gsl_vector_alloc(N_OBSERVATIONS);
    gsl_vector *errors_experim = gsl_vector_alloc(N_OBSERVATIONS);

    gsl_matrix_int *result_by_sigma[N_SIGMAS];
    for (size_t i = 0; i < N_SIGMAS; ++i) {
        result_by_sigma[i] = gsl_matrix_int_calloc(MAX_POWER, MAX_POWER);
    }

    /* Mean values of relative errors of coefficients assuming that power is chosen correctly */
    gsl_matrix *coeffs_errors_by_sigma[N_SIGMAS];
    for (size_t i_sigma = 0; i_sigma < N_SIGMAS; ++i_sigma) {
        coeffs_errors_by_sigma[i_sigma] = gsl_matrix_calloc(MAX_POWER, MAX_POWER + 1);

        /* Mark as NaN errors for coefficients for powers greater than power of the polynomial */
        for (size_t i_poly_power = 0; i_poly_power < MAX_POWER; ++i_poly_power) {
            for (size_t i_power = i_poly_power + 2; i_power < MAX_POWER + 1; ++i_power) {
                gsl_matrix_set(coeffs_errors_by_sigma[i_sigma], i_poly_power, i_power, GSL_NAN);
            }
        }
    }

    gsl_vector_int *powers = gsl_vector_int_alloc(MAX_POWER);
    for (size_t i_power = 0; i_power < MAX_POWER; ++i_power) {
        gsl_vector_int_set(powers, i_power, i_power + 1);
    }

    for (size_t i_power_theor = 0; i_power_theor < MAX_POWER; ++i_power_theor) {
        size_t power_theor = gsl_vector_int_get(powers, i_power_theor);
        gsl_vector *coeffs_theor = gsl_vector_alloc(power_theor + 1);

        for (size_t run = 0; run < N_RUNS; ++run) {
            generate_uniform_sample(generator, coeffs_theor);
            gsl_vector_set(coeffs_theor, power_theor - 1, 10);

            vector_poly_eval(coeffs_theor, times, values_exact);

            generate_sigmas(values_exact, sigmas);

            for (size_t i_sigma = 0; i_sigma < N_SIGMAS; ++i_sigma) {
                const double sigma = gsl_vector_get(sigmas, i_sigma);
                generate_gaussian_sample(0, sigma, generator, errors_theor);
                gsl_vector_memcpy(values, values_exact);
                gsl_vector_add(values, errors_theor);

                for (size_t i_power_experim = 0; i_power_experim < MAX_POWER; ++i_power_experim) {
                    size_t power_experim = gsl_vector_int_get(powers, i_power_experim);
                    gsl_matrix *X = gsl_matrix_alloc(N_OBSERVATIONS, power_experim + 1);
                    calculate_predictor_matrix(times, X);
                    gsl_multifit_linear_svd(X, workspace);

                    gsl_vector *coeffs_experim = gsl_vector_alloc(power_experim + 1);
                    gsl_matrix *cov = gsl_matrix_alloc(power_experim + 1, power_experim + 1);
                    double chisq;
                    gsl_multifit_linear(X, values, coeffs_experim, cov, &chisq, workspace);

                    vector_poly_eval(coeffs_experim, times, values_exact_experim);

                    gsl_vector_memcpy(errors_experim, values);
                    gsl_vector_sub(errors_experim, values_exact_experim);

                    if (test_normal_sarkadi(errors_experim, 0.05) &&
                            test_homogeneity_chisq(errors_experim, 0.05)) {
                        int old = gsl_matrix_int_get(result_by_sigma[i_sigma], i_power_theor, i_power_experim);
                        gsl_matrix_int_set(result_by_sigma[i_sigma], i_power_theor, i_power_experim, old + 1);
                    }

                    if (power_experim == power_theor) {
                        gsl_vector *coeffs_errors = gsl_vector_alloc(power_theor + 1);
                        gsl_vector_memcpy(coeffs_errors, coeffs_theor);
                        gsl_vector_sub(coeffs_errors, coeffs_experim);
                        gsl_vector_div(coeffs_errors, coeffs_theor);

                        for (size_t i = 0; i <= power_experim; ++i) {
                            double old = gsl_matrix_get(coeffs_errors_by_sigma[i_sigma], i_power_theor, i);
                            double addition = fabs(gsl_vector_get(coeffs_errors, i));
                            gsl_matrix_set(coeffs_errors_by_sigma[i_sigma], i_power_theor, i, old + addition);
                        }

                        gsl_vector_free(coeffs_errors);
                    }

                    gsl_matrix_free(cov);
                    gsl_vector_free(coeffs_experim);
                    gsl_matrix_free(X);
                }
            }
        }

        gsl_vector_free(coeffs_theor);
    }

    char *filename = malloc(MAX_FILENAME * sizeof(filename[0]));

    for (size_t i_sigma = 0; i_sigma < N_SIGMAS; ++i_sigma) {
        gsl_matrix_int_scale(result_by_sigma[i_sigma], 100.0 / N_RUNS);
        snprintf(filename, MAX_FILENAME, "report/tables/test_percentage_sigma%zu.tex", i_sigma);
        latex_export_matrix_int(result_by_sigma[i_sigma], "$p_e$", "$p_t$", powers, powers, filename);
    }

    gsl_vector_int *powers_with_zero = gsl_vector_int_alloc(MAX_POWER + 1);

    for (size_t i_power = 0; i_power <= MAX_POWER; ++i_power) {
        gsl_vector_int_set(powers_with_zero, i_power, i_power);
    }

    for (size_t i_sigma = 0; i_sigma < N_SIGMAS; ++i_sigma) {
        gsl_matrix_scale(coeffs_errors_by_sigma[i_sigma], 1.0 / N_RUNS);
        snprintf(filename, MAX_FILENAME, "report/tables/errors_sigma%zu.tex", i_sigma);
        latex_export_matrix(coeffs_errors_by_sigma[i_sigma], "номер\\\\коэфф.",
                            "степ.\\\\многочл.", powers, powers_with_zero, filename);
    }

    gsl_vector_int_free(powers_with_zero);

    free(filename);

    gsl_vector_int_free(powers);

    for (size_t i_sigma = 0; i_sigma < N_SIGMAS; ++i_sigma) {
        gsl_matrix_free(coeffs_errors_by_sigma[i_sigma]);
        gsl_matrix_int_free(result_by_sigma[i_sigma]);
    }

    gsl_vector_free(errors_experim);
    gsl_vector_free(errors_theor);
    gsl_vector_free(sigmas);
    gsl_vector_free(values);
    gsl_vector_free(values_exact_experim);
    gsl_vector_free(values_exact);
    gsl_multifit_linear_free(workspace);
    gsl_rng_free(generator);
    gsl_vector_free(times);
}

void generate_times(double time_from, double time_to, gsl_vector *times) {
    assert(time_from < time_to);

    const double time_step = (time_to - time_from) / (times->size - 1);
    gsl_vector_set(times, 0, time_from);
    for (size_t i = 1; i < times->size; ++i) {
        gsl_vector_set(times, i, gsl_vector_get(times, i - 1) + time_step);
    }
}

void generate_sigmas(const gsl_vector *values_exact, gsl_vector *sigmas) {
    double min_delta = GSL_POSINF, max_delta = 0;
    for (size_t i = 0; i < values_exact->size - 1; ++i) {
        double delta = fabs(gsl_vector_get(values_exact, i + 1) - gsl_vector_get(values_exact, i));
        min_delta = GSL_MIN_DBL(delta, min_delta);
        max_delta = GSL_MAX_DBL(delta, max_delta);
    }

    const double multiplier = pow(max_delta / min_delta, 1.0 / (sigmas->size - 1));
    gsl_vector_set(sigmas, 0, min_delta);
    for (size_t i = 1; i < sigmas->size; ++i) {
        gsl_vector_set(sigmas, i, multiplier * gsl_vector_get(sigmas, i - 1));
    }
}

void calculate_predictor_matrix(const gsl_vector *times, gsl_matrix *X) {
    assert(X->size1 == times->size);

    for (size_t row = 0; row < X->size1; ++row) {
        for (size_t col = 0; col < X->size2; ++col) {
            gsl_matrix_set(X, row, col,
                gsl_pow_int(gsl_vector_get(times, row), col));
        }
    }
}
