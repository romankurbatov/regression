TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

DEFINES += HAVE_INLINE GSL_C99_INLINE
QMAKE_CFLAGS += -std=c99
LIBS += -lgsl -lgslcblas -lm

SOURCES += \
        chisq.c \
        generate.c \
        latex.c \
        main.c \
        sarkadi.c \
        vector_util.c

HEADERS += \
    chisq.h \
    generate.h \
    latex.h \
    sarkadi.h \
    vector_util.h
